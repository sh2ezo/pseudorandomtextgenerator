﻿using System;
using System.Collections.Generic;
using PseudoRandomTextGenerator.Compilation;
using PseudoRandomTextGenerator.Tokens;

namespace PseudoRandomTextGenerator
{
    /// <summary>
    /// Class used to generate text based on the specified template
    /// </summary>
    public class TextGenerator
    {
        /// <summary>
        /// Collection of the named value generators
        /// </summary>
        private readonly Dictionary<string, Func<string>> _valueGenerators = new Dictionary<string, Func<string>>();

        /// <summary>
        /// Generator of the random numbers used for the text generation
        /// </summary>
        private readonly Random _random = new Random();

        /// <summary>
        /// Delegate built from the provided template
        /// </summary>
        private readonly Func<Func<string, string>, Random, string> _generatorFunc;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="template">Template the generation based on</param>
        public TextGenerator(string template)
        {
            var charStream = new CharStream(template);
            var tokens = Tokenizer.Tokenize(charStream);
            _generatorFunc = Compiler.Compile(tokens);
        }

        /// <summary>
        /// Generates text using the template provided during the construction of the instance
        /// </summary>
        /// <returns></returns>
        public string Generate()
        {
            return _generatorFunc.Invoke(GetVariableValue, _random);
        }

        /// <summary>
        /// Generates text using template provided during the construction of the instance.
        /// </summary>
        /// <param name="values">Values of the external variables</param>
        public string Generate(IReadOnlyDictionary<string, string> values)
        {
            return _generatorFunc.Invoke(n =>
            {
                if (values.TryGetValue(n, out var value))
                {
                    return value;
                }

                return GetVariableValue(n);
            }, _random);
        }

        /// <summary>
        /// Generates text using template provided during the construction of the instance.
        /// </summary>
        /// <param name="generators">Generators of the values of the external variables</param>
        public string Generate(IReadOnlyDictionary<string, Func<string>> generators)
        {
            return _generatorFunc.Invoke(n =>
            {
                if (generators.TryGetValue(n, out var generator))
                {
                    return generator?.Invoke();
                }

                return GetVariableValue(n);
            }, _random);
        }

        private string GetVariableValue(string variableName)
        {
            if (!_valueGenerators.TryGetValue(variableName, out var generator))
            {
                throw new GeneratorNotFoundException("generator not found for the variable with name: " + variableName);
            }

            return generator.Invoke();
        }

        /// <summary>
        /// Updates generator used to acquire values for variable with specified <paramref name="name"/>
        /// </summary>
        /// <param name="name">Name of variable</param>
        /// <param name="valueGenerator">Function used to generate variable value</param>
        public void UpdateNamedVariableValueGenerator(string name, Func<string> valueGenerator)
        {
            _valueGenerators[name] = valueGenerator ?? throw new NullReferenceException($"{nameof(valueGenerator)} must not be null");
        }
    }
}
