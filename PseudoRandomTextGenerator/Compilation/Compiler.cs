﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using PseudoRandomTextGenerator.Tokens;

namespace PseudoRandomTextGenerator.Compilation
{
    internal static class Compiler
    {
        private static readonly MethodInfo StringBuilderAppendStringMethod = typeof(StringBuilder).GetMethod("Append", new[] {
            typeof(string)
        });

        private static readonly MethodInfo RandomNextMethod = typeof(Random).GetMethod("Next", new[]
        {
            typeof(int)
        });

        private static readonly MethodInfo FuncInvokeMethod = typeof(Func<string, string>).GetMethod("Invoke", new []
        {
            typeof(string)
        });

        private static readonly MethodInfo StringBuilderToStringMethod = typeof(StringBuilder).GetMethod("ToString", Array.Empty<Type>());

        private class CompilerContext
        {
            public CompilerContext(ParameterExpression variableHandlerExpression, ParameterExpression resultExpression, Queue<Token> tokens, ParameterExpression randomGeneratorExpression)
            {
                VariableHandlerExpression = variableHandlerExpression;
                ResultExpression = resultExpression;
                Tokens = tokens;
                RandomGeneratorExpression = randomGeneratorExpression;
            }

            public ParameterExpression VariableHandlerExpression { get; }
            public ParameterExpression ResultExpression { get; }
            public ParameterExpression RandomGeneratorExpression { get; }
            public Queue<Token> Tokens { get; }
        };

        public static Func<Func<string, string>, Random, string> Compile(Queue<Token> tokens)
        {
            // compile the built expression into the delegate
            return BuildExpression(tokens).Compile();
        }

        public static Expression<Func<Func<string, string>, Random, string>> BuildExpression(Queue<Token> tokens)
        {
            var variableHandlerExpression = Expression.Parameter(typeof(Func<string, string>), "variableHandler");
            var resultExpression = Expression.Variable(typeof(StringBuilder), "resultBuilder");
            var randomExpression = Expression.Parameter(typeof(Random), "random");
            var returnTarget = Expression.Label(typeof(string));

            Debug.Assert(StringBuilderAppendStringMethod != null, nameof(StringBuilderAppendStringMethod) + " != null");

            var body = new List<Expression>
            {
                Expression.Assign(
                    resultExpression,
                    Expression.New(typeof(StringBuilder))
                ),
                BuildExpression(
                    new CompilerContext(variableHandlerExpression, resultExpression, tokens, randomExpression)
                ),
                Expression.Return(
                    returnTarget,
                    Expression.Call(resultExpression, StringBuilderToStringMethod)
                ),
                Expression.Label(
                    returnTarget, 
                    Expression.Default(typeof(string))
                )
            };

            // check if there are tokens left
            if (tokens.TryPeek(out var nextToken))
            {
                if (nextToken.Type == TokenType.CloseBrace)
                {
                    throw new CompilerException("closing bracket without the opening one", nextToken.Line, nextToken.Column);
                }
            }

            // reduce the expression
            var generatorBlockExpression = Expression.Block(new[] { resultExpression }, body) as Expression;

            while (generatorBlockExpression.CanReduce)
            {
                generatorBlockExpression = generatorBlockExpression.Reduce();
            }

            // build the lambda expression
            return Expression.Lambda<Func<Func<string, string>, Random, string>>(
                generatorBlockExpression, 
                variableHandlerExpression, 
                randomExpression
            );
        }

        private static Expression BuildExpression(CompilerContext compilerContext)
        {
            var expressions = new List<Expression>();
            var currentExpression = new List<Expression>();

            while (compilerContext.Tokens.Count > 0)
            {
                var token = compilerContext.Tokens.Peek();

                if (token.Type == TokenType.CloseBrace)
                {
                    break;
                }
                else if (token.Type == TokenType.RandomSwitch)
                {
                    currentExpression.Add(Expression.Empty());
                    expressions.Add(Expression.Block(currentExpression));
                    currentExpression.Clear();
                }
                else if (token.Type == TokenType.Constant)
                {
                    var constExpression = Expression.Constant(token.Text);
                    var appendConstExpression = Expression.Call(compilerContext.ResultExpression,
                        StringBuilderAppendStringMethod, constExpression);
                    currentExpression.Add(appendConstExpression);
                }
                else if (token.Type == TokenType.OpenBrace)
                {
                    compilerContext.Tokens.Dequeue();
                    var expression = BuildExpression(compilerContext);
                    currentExpression.Add(expression);

                    if (!compilerContext.Tokens.TryPeek(out var nextToken) || nextToken.Type != TokenType.CloseBrace)
                    {
                        throw new CompilerException("no closing bracket for the opened one", token.Line, token.Column);
                    }
                }
                else if (token.Type == TokenType.Variable)
                {
                    var variableNameExpression = Expression.Constant(token.Text);
                    var getVariableValueExpression =
                        Expression.Call(compilerContext.VariableHandlerExpression, FuncInvokeMethod, variableNameExpression);
                    var appendExpression = Expression.Call(compilerContext.ResultExpression,
                        StringBuilderAppendStringMethod, getVariableValueExpression);
                    currentExpression.Add(appendExpression);
                }

                compilerContext.Tokens.Dequeue();
            }

            // if current expression is not empty construct block from it
            if (currentExpression.Count > 0)
            {
                currentExpression.Add(Expression.Empty());
                expressions.Add(Expression.Block(currentExpression));
            }

            // if list of expressions is empty return empty expression
            if (expressions.Count == 0)
            {
                return Expression.Empty();
            }

            // if list of expressions contains only one expression return it
            if (expressions.Count == 1)
            {
                return expressions[0];
            }

            // generate random switch block
            var optionsNumberExpression = Expression.Constant(expressions.Count);
            var getRandomNumberExpression = Expression.Call(compilerContext.RandomGeneratorExpression, RandomNextMethod,
                optionsNumberExpression);
            var caseExpressions = new List<SwitchCase>();

            for (int i = 0; i < expressions.Count; i += 1)
            {
                caseExpressions.Add(Expression.SwitchCase(
                    expressions[i],
                    Expression.Constant(i)
                ));
            }

            return Expression.Switch(getRandomNumberExpression, Expression.Empty(), caseExpressions.ToArray());
        }
    }
}
