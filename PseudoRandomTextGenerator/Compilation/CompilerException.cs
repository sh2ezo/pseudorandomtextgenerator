﻿using System;

namespace PseudoRandomTextGenerator.Compilation
{
    /// <summary>
    /// Exception raised when the provided template can't be compiled
    /// </summary>
    public class CompilerException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CompilerException(string message, int line, int column) : base(message)
        {
            Line = line;
            Column = column;
        }

        /// <summary>
        /// Line wrong token present in
        /// </summary>
        public int Line { get; }

        /// <summary>
        /// Column wrong token present in
        /// </summary>
        public int Column { get; }
    }
}
