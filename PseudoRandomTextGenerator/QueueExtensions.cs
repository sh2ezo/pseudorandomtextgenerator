﻿using System.Collections.Generic;

namespace PseudoRandomTextGenerator
{
    internal static class QueueExtensions
    {
        public static bool TryPeek<T>(this Queue<T> queue, out T value)
        {
            if (queue.Count == 0)
            {
                value = default(T);
                return false;
            }

            value = queue.Peek();
            return true;
        }
    }
}
