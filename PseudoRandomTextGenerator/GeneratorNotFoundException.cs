﻿using System;

namespace PseudoRandomTextGenerator
{
    /// <summary>
    /// Exception raised when generator for the variable was not found
    /// </summary>
    public class GeneratorNotFoundException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message"></param>
        public GeneratorNotFoundException(string message) : base(message)
        {
        }
    }
}
