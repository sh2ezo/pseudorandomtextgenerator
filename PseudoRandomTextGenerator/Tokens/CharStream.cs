﻿namespace PseudoRandomTextGenerator.Tokens
{
    internal class CharStream
    {
        private readonly string _text;
        private int _offset;
        private int _line;
        private int _column;

        public CharStream(string text)
        {
            _text = text;
        }

        public bool TryPeek(out CharInfo charInfo)
        {
            if (_offset >= _text.Length)
            {
                charInfo = default;
                return false;
            }

            charInfo = new CharInfo(_text[_offset], _line, _column);
            return true;
        }

        public void ReadNext()
        {
            if (_offset >= _text.Length)
            {
                return;
            }

            if (_text[_offset] == '\n')
            {
                _line += 1;
                _column = 0;
            }
            else
            {
                _column += 1;
            }

            _offset += 1;
        }
    }
}
