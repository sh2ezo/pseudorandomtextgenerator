﻿using System.Collections.Generic;
using System.Text;

namespace PseudoRandomTextGenerator.Tokens
{
    internal static class Tokenizer
    {
        public static Queue<Token> Tokenize(CharStream stream)
        {
            var tokens = new Queue<Token>();

            while (stream.TryPeek(out var @char))
            {
                if (@char == '{')
                {
                    tokens.Enqueue(new Token(TokenType.OpenBrace, @char.Line, @char.Column));
                }
                else if(@char == '}')
                {
                    tokens.Enqueue(new Token(TokenType.CloseBrace, @char.Line, @char.Column));
                }
                else if (@char == '|')
                {
                    tokens.Enqueue(new Token(TokenType.RandomSwitch, @char.Line, @char.Column));
                }
                else if (@char == '$')
                {
                    stream.ReadNext();
                    var name = ReadName(stream);
                    tokens.Enqueue(new Token(name, TokenType.Variable, @char.Line, @char.Column));
                    continue;
                }
                else
                {
                    var text = ReadConst(stream);
                    tokens.Enqueue(new Token(text, TokenType.Constant, @char.Line, @char.Column));
                    continue;
                }

                stream.ReadNext();
            }

            return tokens;
        }

        private static string ReadName(CharStream stream)
        {
            var name = new StringBuilder();

            while (stream.TryPeek(out var @char))
            {
                if ((@char >= 'a' && @char <= 'z') ||
                    (@char >= 'A' && @char <= 'Z') ||
                    (@char >= '0' && @char <= '9') ||
                    @char == '_')
                {
                    name.Append(@char.Char);
                }
                else
                {
                    break;
                }

                stream.ReadNext();
            }

            return name.ToString();
        }

        private static string ReadConst(CharStream stream)
        {
            var @const = new StringBuilder();

            while (stream.TryPeek(out var @char))
            {
                if (@char == '\\') // escape character
                {
                    stream.ReadNext();

                    if (!stream.TryPeek(out var escapedChar))
                    {
                        throw new TokenizerException("no character to escape", @char.Line, @char.Column);
                    }

                    @char = escapedChar;
                }
                else if (@char == '{' || @char == '}' || @char == '|' || @char == '$')
                {
                    break;
                }

                @const.Append(@char.Char);
                stream.ReadNext();
            }

            return @const.ToString();
        }
    }
}
