﻿namespace PseudoRandomTextGenerator.Tokens
{
    internal struct Token
    {
        public readonly string Text;
        public readonly TokenType Type;
        public readonly int Column;
        public readonly int Line;

        public Token(string text, TokenType type, int column, int line)
        {
            Text = text;
            Type = type;
            Column = column;
            Line = line;
        }

        public Token(TokenType type, int line, int column)
        {
            Type = type;
            Line = line;
            Column = column;
            Text = null;
        }
    }
}
