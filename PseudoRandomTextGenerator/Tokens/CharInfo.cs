﻿namespace PseudoRandomTextGenerator.Tokens
{
    internal struct CharInfo
    {
        public readonly char Char;
        public readonly int Line;
        public readonly int Column;

        public CharInfo(char c, int line, int column)
        {
            Char = c;
            Line = line;
            Column = column;
        }

        public static bool operator ==(CharInfo a, char b)
        {
            return a.Char == b;
        }

        public static bool operator !=(CharInfo a, char b)
        {
            return a.Char != b;
        }

        public static bool operator >(CharInfo a, char b)
        {
            return a.Char > b;
        }

        public static bool operator <(CharInfo a, char b)
        {
            return a.Char < b;
        }

        public static bool operator <=(CharInfo a, char b)
        {
            return a.Char <= b;
        }

        public static bool operator >=(CharInfo a, char b)
        {
            return a.Char >= b;
        }

        public bool Equals(CharInfo other)
        {
            return Char == other.Char && Line == other.Line && Column == other.Column;
        }

        public override bool Equals(object obj)
        {
            return obj is CharInfo other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Char.GetHashCode();
                hashCode = (hashCode * 397) ^ Line;
                hashCode = (hashCode * 397) ^ Column;
                return hashCode;
            }
        }

    }
}
