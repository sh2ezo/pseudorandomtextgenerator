﻿namespace PseudoRandomTextGenerator.Tokens
{
    internal enum TokenType
    {
        OpenBrace,
        CloseBrace,
        Constant,
        RandomSwitch,
        Variable
    }
}
