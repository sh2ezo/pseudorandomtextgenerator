﻿using System;

namespace PseudoRandomTextGenerator.Tokens
{
    /// <summary>
    /// Exception raised when wrong character is met
    /// </summary>
    public class TokenizerException: Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TokenizerException(string message, int line, int column) : base(message)
        {
            Line = line;
            Column = column;
        }

        /// <summary>
        /// Line wrong character is met in
        /// </summary>
        public int Line { get; }
        /// <summary>
        /// Column wrong character is met in
        /// </summary>
        public int Column { get; }
    }
}
