# PseudoRandomTextGenerator

The main purpose of this library is to allow its user to generate the pseudo-random text using the provided template.

## Examples of templates

 * abc|def|ghi - generates 'abc', 'def', or 'ghi'
 * {a|b}{1|2} - generates 'a1', 'a2', 'b1', or 'b2'
 * $var - generates string containing the value of the external variable 'var'
 * \\{\\\$\\} - generates '{$}'
 * {a|b}|{1|2} - generates 'a', 'b', '1', or '2'

## Example of usage

```csharp
using System;
using System.Globalization;
using PseudoRandomTextGenerator;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var generator =
                new TextGenerator("{Hello!|Hi!|Hey, man!}. Do {u|you} {want|wanna} to walk today at $time?");

            generator.UpdateNamedVariableValueGenerator("time", () => DateTime.Now.ToString(CultureInfo.InvariantCulture));

            for (int i = 0; i < 1000; i += 1)
            {
                Console.WriteLine(generator.Generate());
            }
        }
    }
}
```