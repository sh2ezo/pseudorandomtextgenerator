﻿using System;
using System.Globalization;
using PseudoRandomTextGenerator;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var generator =
                new TextGenerator("{Hello!|Hi!|Hey, man!}. Do {u|you} {want|wanna} to walk today at $time?");

            generator.UpdateNamedVariableValueGenerator("time", () => DateTime.Now.ToString(CultureInfo.InvariantCulture));

            for (int i = 0; i < 1000; i += 1)
            {
                Console.WriteLine(generator.Generate());
            }
        }
    }
}
